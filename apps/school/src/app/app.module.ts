import {Module} from '@nestjs/common';

import {AppController} from './app.controller';
import {AppService} from './app.service';
import {StudentModule} from './student/student.module';
import {ClassroomModule} from './classroom/classroom.module';
import {MongooseModule} from "@nestjs/mongoose";
import {ClassModule} from './class/class.module';
import {TeacherModule} from './teacher/teacher.module';

@Module({
  imports: [
    StudentModule,
    ClassroomModule,
    ClassModule,
    TeacherModule,
    MongooseModule.forRoot(
      'mongodb://127.0.0.1:27017/school'
    )
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
