import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {ClassRequest} from "./class.request";
import {ClassService} from "./class.service";

@Controller('class')
export class ClassController {
  constructor(private service: ClassService) {
  }

  @Post()
  async add(@Body() req: ClassRequest) {
    await this.service.add(req);
  }

  @Get()
  async getAll() {
    return await this.service.getAll()
  }

  @Get('/:name')
  async getByName(
    @Param('name') name: string) {
    return await this.service.getByName(name)
  }
}
