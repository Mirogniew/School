import {Module} from '@nestjs/common';
import {ClassService} from './class.service';
import {ClassController} from './class.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {ClassSchema} from "./class.schema";
import {ClassRepository} from "./class.repository";
import {TeacherModule} from "../teacher/teacher.module";

@Module({
  imports: [
    TeacherModule,
    MongooseModule.forFeature([
      {name: 'Class', schema: ClassSchema}
    ])
  ],
  controllers: [ClassController],
  providers: [ClassService, ClassRepository]
})
export class ClassModule {
}
