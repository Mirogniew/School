import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Class} from "./class.interface";
import {ClassRequest} from "./class.request";

export class ClassRepository {
  constructor(
    @InjectModel('Class') private model: Model<Class>
  ) {
  }

  async find(): Promise<Class[]> {
    return this.model.find();
  }

  async save(request: ClassRequest) {
    const record = new this.model(request);
    await record.save()
  }

  async findByName(name: string) {
    return this.model.findOne({
      name: name
    });
  }
}
