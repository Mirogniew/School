import {Injectable} from '@nestjs/common';
import {Class} from "./class.interface";
import {ClassRequest} from "./class.request";
import {ClassRepository} from "./class.repository";
import {TeacherRepository} from "../teacher/teacher.repository";

@Injectable()
export class ClassService {
  constructor(
    private repository: ClassRepository,
    private teacherRepository: TeacherRepository
  ) {
  }

  async add(request: ClassRequest) {
    await this.repository.save(request);
  }

  async getAll(): Promise<Class[]> {
    return await this.repository.find();
  }

  async getByName(name: string) {
    const course = await this.repository.findByName(name);
    const teacher = await this.teacherRepository.findByClass(course.id);
    return {
      id: course.id,
      name: course.name,
      teacher: teacher
    };
  }
}
