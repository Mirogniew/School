import {Body, Controller, Get, Post} from '@nestjs/common';
import {ClassroomService} from "./classroom.service";
import {ClassroomRequest} from "./classroom.request";

@Controller('classroom')
export class ClassroomController {
  constructor(private service: ClassroomService) {
  }

  @Get()
  async getAll() {
    return await this.service.getAll()
  }

  @Post()
  async add(@Body() req: ClassroomRequest) {
    await this.service.add(req);
  }
}
