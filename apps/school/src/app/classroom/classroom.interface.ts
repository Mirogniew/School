import {Document, Types} from 'mongoose';

export interface Classroom extends Document {
  __id: Types.ObjectId
  class_number: number
  capacity: number
  class_length: number
  class_width: number
}
