import {Module} from '@nestjs/common';
import {ClassroomController} from './classroom.controller';
import {ClassroomService} from './classroom.service';
import {ClassroomRepository} from './classroom.repository';
import {MongooseModule} from "@nestjs/mongoose";
import {ClassroomSchema} from "./classroom.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
        {name: 'Classroom', schema: ClassroomSchema}
      ]
    )
  ],
  controllers: [ClassroomController],
  providers: [ClassroomService, ClassroomRepository]
})
export class ClassroomModule {
}
