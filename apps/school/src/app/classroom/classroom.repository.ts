import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {ClassroomRequest} from "./classroom.request";
import {Classroom} from "./classroom.interface";

@Injectable()
export class ClassroomRepository {
  constructor(
    @InjectModel('Classroom') private model: Model<Classroom>
  ) {
  }

  async find(): Promise<Classroom[]> {
    return this.model.find();
  }

  async save(request: ClassroomRequest) {
    const record = new this.model(request);
    await record.save();
  }
}
