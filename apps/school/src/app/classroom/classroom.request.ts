export class ClassroomRequest {
  class_number: number
  capacity: number
  class_length: number
  class_width: number
}
