import mongoose from "mongoose";

export const ClassroomSchema = new mongoose.Schema({
  __id: String,
  number: Number,
  capacity: Number,
  length: Number,
  width: Number,
})
