import {Injectable} from '@nestjs/common';
import {ClassroomRepository} from "./classroom.repository";
import {ClassroomRequest} from "./classroom.request";
import {Classroom} from "./classroom.interface";

@Injectable()
export class ClassroomService {
  constructor(private repository: ClassroomRepository) {
  }

  async add(request: ClassroomRequest) {
    await this.repository.save(request);
  }

  async getAll(): Promise<Classroom[]> {
    return await this.repository.find();
  }
}
