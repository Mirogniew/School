import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {StudentService} from "./student.service";
import {StudentRequest} from "./student.request";

@Controller('student')
export class StudentController {
  constructor(private service: StudentService) {
  }

  @Post()
  async add(@Body() req: StudentRequest) {
    await this.service.add(req);
  }

  @Get()
  async getAll() {
    return await this.service.getAll()
  }

  @Get('/:id')
  async getByName(
    @Param('id') id: string) {
    return await this.service.getByStudentId(id);
  }

}
