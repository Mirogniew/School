import {Document, Types} from 'mongoose';

export interface Student extends Document {
  _id: Types.ObjectId;
  class_id: Types.ObjectId,
  firstName: string,
  middleName: string,
  lastName: string,
  dayOfBirth: string,
}
