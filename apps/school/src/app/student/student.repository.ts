import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Student} from "./student.interface";
import {Model} from "mongoose";
import {StudentRequest} from "./student.request";

@Injectable()
export class StudentRepository {
  constructor(
    @InjectModel('Student') private model: Model<Student>
  ) {
  }

  async find(): Promise<Student[]> {
    return this.model.find();
  }

  async findByStudentId(id: string) {
    return this.model.findOne({
      _id: id
    })
  }

  async save(request: StudentRequest) {
    const record = new this.model(request);
    await record.save()
  }
}
