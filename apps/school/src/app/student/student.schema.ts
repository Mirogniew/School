import * as mongoose from "mongoose";


export const StudentSchema = new mongoose.Schema({
  __id: String,
  class_id: String,
  firstName: String,
  middleName: String,
  lastName: String,
  dayOfBirth: String,
})
