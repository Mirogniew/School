import {Injectable} from '@nestjs/common';
import {StudentRequest} from "./student.request";
import {StudentRepository} from "./student.repository";
import {Student} from "./student.interface";

@Injectable()
export class StudentService {
  constructor(private repository: StudentRepository) {
  }

  async add(request: StudentRequest) {
    await this.repository.save(request);
  }

  async getAll(): Promise<Student[]> {
    return await this.repository.find();
  }

  async getByStudentId(id: string) {
    return await this.repository.findByStudentId(id);
  }
}
