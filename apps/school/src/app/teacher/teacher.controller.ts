import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {TeacherService} from "./teacher.service";
import {TeacherRequest} from "./teacher.request";

@Controller('teacher')
export class TeacherController {
  constructor(private service: TeacherService) {
  }

  @Post()
  async add(@Body() req: TeacherRequest) {
    await this.service.add(req);
  }

  @Get()
  async getAll() {
    return await this.service.getAll()
  }

  @Get('/:course')
  async getByCourse(
    @Param('course') course: string) {
    return await this.service.getByClass(course);
  }

  @Get('/:teacher')
  async getBySurName(
    @Param('teacher') teacher: string) {
    return await this.service.getByClass(teacher);
  }
}
