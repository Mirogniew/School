import {Document, Types} from 'mongoose';

export interface Teacher extends Document {
  _id: Types.ObjectId;
  class_id: Types.ObjectId
  name: string
  lastName: string
  dayOfBirth: string
  subject: Array<string>
}
