import {Module} from '@nestjs/common';
import {TeacherController} from './teacher.controller';
import {TeacherService} from './teacher.service';
import {TeacherRepository} from './teacher.repository';
import {MongooseModule} from "@nestjs/mongoose";
import {TeacherSchema} from "./teacher.schema";

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: 'Teacher', schema: TeacherSchema}
    ])
  ],
  controllers: [TeacherController],
  providers: [TeacherService, TeacherRepository]
})
export class TeacherModule {
}
