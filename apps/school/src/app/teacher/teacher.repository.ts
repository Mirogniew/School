import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Teacher} from "./teacher.interface";
import {TeacherRequest} from "./teacher.request";

@Injectable()
export class TeacherRepository {
  constructor(
    @InjectModel('Teacher') private model: Model<Teacher>
  ) {
  }

  async find(): Promise<Teacher[]> {
    return this.model.find();
  }

  async findBySurname(teacher: string) {
    return this.model.findOne({
      lastName: teacher
    })
  }

  async save(request: TeacherRequest) {
    const record = new this.model(request);
    await record.save()
  }

  async findByClass(course: string) {
    return this.model.findOne({
      class_id: course
    })
  }
}
