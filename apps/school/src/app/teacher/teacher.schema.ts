import mongoose from "mongoose";

export const TeacherSchema = new mongoose.Schema({
  __id: String,
  class_id: String,
  firstName: String,
  middleName: String,
  lastName: String,
  dayOfBirth: String,
})
