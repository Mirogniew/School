import {Injectable} from '@nestjs/common';
import {TeacherRepository} from "./teacher.repository";
import {TeacherRequest} from "./teacher.request";
import {Teacher} from "./teacher.interface";

@Injectable()
export class TeacherService {
  constructor(private repository: TeacherRepository) {
  }

  async add(request: TeacherRequest) {
    await this.repository.save(request);
  }

  async getAll(): Promise<Teacher[]> {
    return await this.repository.find();
  }

  async getByClass(course: string) {
    return await this.repository.findByClass(course);
  }

  async getBySurname(teacher: string) {
    return await this.repository.findBySurname(teacher);
  }
}
